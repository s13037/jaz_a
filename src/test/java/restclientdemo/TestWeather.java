package restclientdemo;

import static org.junit.Assert.*;

import org.junit.Test;
import weather.*;
public class TestWeather {

	@Test
	public void fahrenheit_to_celcius_conversion() {
		Weather w = new Weather();
		w.setTemperatureFahrenheit(70);
		assertEquals(21.1f, w.getTemperatureCelcius(), 0.1f);
	}

}
