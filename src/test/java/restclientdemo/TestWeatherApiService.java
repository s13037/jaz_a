package restclientdemo;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import services.WeatherApiService;
import services.weather.WeatherResponse;
import weather.Weather;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
public class TestWeatherApiService extends Mockito{

	@Test
	public void api_should_throw_exeption_when_incorrent_code() {
		try{
			WeatherApiService api = new WeatherApiService();
			api.getWeatherFromCityId(0);
			fail("Exception not thrown");
		}catch(NullPointerException e){
			return;
		}
		catch(IOException e){
			return;
		}
	}
	
	@Test
	public void api_should_process_every_json_element_needed() throws JsonParseException, JsonMappingException, IOException {
		String jsonValue = "{\"coord\":{ \"lon\":19.94,\"lat\":50.06},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03n\"}],\"base\":\"cmc stations\",\"main\":{\"temp\":290.581,\"pressure\":983.46,\"humidity\":53,\"temp_min\":290.581,\"temp_max\":290.581,\"sea_level\":1023.06,\"grnd_level\":983.46},\"wind\":{\"speed\":3.11,\"deg\":92.5004},\"clouds\":{\"all\":44},\"dt\":1462992008,\"sys\":{\"message\":0.0031,\"country\":\"PL\",\"sunrise\":1462935500,\"sunset\":1462990547},\"id\":3085041,\"name\":\"Śródmieście\",\"cod\":200}";
		WeatherApiService api = new WeatherApiService();
		Weather w = api.getWeatherFromJson(jsonValue);
		assertEquals((float)983.46f, w.getPressure(), 0.1);
		assertEquals((float)3.11, w.getWindSpeed(), 0.1);
		assertEquals((float)44, w.getCloudiness(), 0.1);
		assertEquals((float)290.5, w.getTemperatureFahrenheit(), 0.1);
		}
	
}
