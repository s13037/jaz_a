﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pogoda</title>
</head>
<body>

<jsp:useBean id="weather" class="weather.Weather" scope="session" />

<jsp:useBean id="apiService" class="services.WeatherApiService" scope="application" />

<%
int id = Integer.parseInt(request.getParameter("id"));
weather = apiService.getWeatherFromCityId(id);
%>
Ciśnienie: <% out.println(weather.getPressure()); %> hPa</br>
Zachmurzenie: <% out.println(weather.getCloudiness()); %> % </br>
Temperatura: <% out.println(weather.getTemperatureCelcius()); %> C</br>
Prędkość wiatru: <% out.println(weather.getWindSpeed()); %> m/s</br>
</body>
</html>