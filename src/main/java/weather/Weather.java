package weather;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Weather {
	float temperatureFahrenheit = 0;
	float pressure = 0;
	float humidity = 0;
	float temperatureCelcius = 0;
	float windSpeed = 0;
	float cloudiness = 0;

	String cityName;
	public String getCityName() {
		return cityName;
	}

	public Weather(){}
	
	public float getCloudiness() {
		return cloudiness;
	}
	public void setCloudiness(float cloudiness) {
		this.cloudiness = cloudiness;
	}
	public void setTemperatureFahrenheit(float temperatureFahrenheit) {
		this.temperatureFahrenheit = temperatureFahrenheit;
		this.temperatureCelcius = (temperatureFahrenheit - (float)32.0)/ (float)1.8;
	}
	public void setPressure(float pressure) {
		this.pressure = pressure;
	}
	public void setHumidity(float humidity) {
		this.humidity = humidity;
	}
	public float getTemperatureCelcius() {
		return temperatureCelcius;
	}
	public float getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(float windSpeed) {
		this.windSpeed = windSpeed;
	}
	public float getTemperatureFahrenheit() {
		return temperatureFahrenheit;
	}
	public float getPressure() {
		return pressure;
	}
	public float getHumidity() {
		return humidity;
	}
	
	
}
