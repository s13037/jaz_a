package services;

import java.io.IOException;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.sun.jersey.*;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;

import services.weather.*;
import weather.*;

public class WeatherApiService {
	String url = "http://api.openweathermap.org/data/2.5/weather?APPID=4baad63e9037606e62f8c14123694ea5";

	public Weather getWeatherFromCityId(int id) throws JsonParseException, JsonMappingException, IOException {
		ClientConfig clientConfig = new DefaultClientConfig();
		Client client = Client.create(clientConfig);

		WebResource webResource = client.resource(url + "&id=" + Integer.toString(id));
		String response = webResource.get(String.class);
		if (response.length() < 1)
			throw new IOException();

		return getWeatherFromJson(response);
	}

	public Weather getWeatherFromJson(String json) throws JsonParseException, JsonMappingException, IOException {

		ObjectMapper mapper = new ObjectMapper();
		WeatherResponse res = mapper.readValue(json, WeatherResponse.class);
		Weather weather = new Weather();

		weather.setPressure(res.getMain().getPressure());
		weather.setTemperatureFahrenheit(res.getMain().getTemp());
		weather.setWindSpeed(res.getWind().getSpeed());
		weather.setCloudiness(res.getClouds().getAll());
		return weather;
	}
}
