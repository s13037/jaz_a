package services.weather;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Clouds {
	public float getAll() {
		return all;
	}

	public void setAll(float all) {
		this.all = all;
	}

	public float all = 0;
}
